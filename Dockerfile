FROM python:3


RUN apt-get -y update && \
    apt-get upgrade -y && \
    apt-get install -y python3-dev &&\
    apt-get update -y



## install prerequisites
RUN pip3 install Flask
WORKDIR /broker
ADD flaskController.py /broker


EXPOSE 8080

ENTRYPOINT FLASK_APP=flaskController.py flask run --host=0.0.0.0 --port=8080

