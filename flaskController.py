#!/usr/bin/python3
from flask import Flask, flash, request, redirect, url_for
import os
import json


app = Flask(__name__)
config = open("/etc/qurator/index.yaml")
page="<html>"

tpl="""<a href="%s">%s</a><br>
"""

for line in config:
  service, host = line.strip().split(":")
  page+=tpl%(host, service)
page+="</html>"
    
@app.route('/', methods=['GET'])
def dummy():
    return page

    
if __name__ == '__main__':
    app.run(host='localhost', port=8080, debug=True)
